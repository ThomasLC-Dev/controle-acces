package fr.epsi.controleacces.models;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.UUID;

@AllArgsConstructor
@Getter
public class Badge{
    private String uid;
    private boolean bloque = false;

    public Badge() {
        this.uid = UUID.randomUUID().toString();;
    }

    public void bloquer() {
        this.bloque = true;
    }

    public void debloquer() {
        this.bloque = false;
    }

}
