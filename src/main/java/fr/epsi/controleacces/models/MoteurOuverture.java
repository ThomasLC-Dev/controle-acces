package fr.epsi.controleacces.models;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class MoteurOuverture {

    private List<ILecteur> lecteurs;
    private IMoteurOuvertureLogger logger;

    public MoteurOuverture(List<ILecteur> lecteurs) {
        this.lecteurs = lecteurs;
    }
     public void interrogerLecteurs() {
         try {
             for (ILecteur lecteur : lecteurs) {
                 if (lecteur.isBadgeDetecte()) {
                     for(IPorte porte : lecteur.getPortes()){
                         try{
                             porte.ouvrir();
                             logger.logOuverture(porte.getUid(), lecteur, true);
                         }catch(Exception e){
                             logger.logOuverture(porte.getUid(), lecteur, false);
                             logger.logException(e);
                         }
                     }
                 }else{
                     logger.logOuverture("Aucune porte", lecteur, false);
                 }
             }
         } catch (Exception e) {
             logger.logException(e);
         }
     }

     public ILecteur getLecteur(String uidLecteur) {
         for (ILecteur lecteur : lecteurs) {
             if (lecteur.getUid().equals(uidLecteur)) {
                 return lecteur;
             }
         }
         return null;
     }
}
