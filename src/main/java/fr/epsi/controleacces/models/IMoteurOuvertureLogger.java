package fr.epsi.controleacces.models;

public interface IMoteurOuvertureLogger {
    void logException(Exception e);
    void logOuverture(String uidPorte, ILecteur lecteur, boolean ouvertureReussie);
    boolean isExceptionDansLesLogs();
    String getLog(String uidPorte);
}
