package fr.epsi.controleacces.models;


import java.util.List;

public interface ILecteur {
    boolean isBadgeDetecte();

    List<IPorte> getPortes();
    String getUid();

    void simulerDetectionBadge();
    void simulerDetectionBadge(Badge badge);

    IPorte getPorte(String porteValide);
    String getBadgeUid();
}
