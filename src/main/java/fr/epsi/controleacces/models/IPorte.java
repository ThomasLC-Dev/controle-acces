package fr.epsi.controleacces.models;

public interface IPorte {
    void ouvrir();
    boolean getARecuUnSignalDOuverture();
    String getUid();
}
