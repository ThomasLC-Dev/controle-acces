package fr.epsi.controleacces.utilities;

import fr.epsi.controleacces.models.ILecteur;
import fr.epsi.controleacces.models.IMoteurOuvertureLogger;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class LoggerSpy implements IMoteurOuvertureLogger {
    private boolean ExceptionDansLesLogs;
    private boolean OuvertureDansLesLogs;
    private boolean ErreurOuvertureDansLesLogs;
    private List<String> logs = new ArrayList<>();
    public void logException(Exception e) {
        ExceptionDansLesLogs = true;
    }
    public void logOuverture(String uidPorte, ILecteur lecteur, boolean ouvertureReussie) {
        if(ouvertureReussie){
            journaliserOuverture(uidPorte, lecteur.getUid(), lecteur.getBadgeUid());
            OuvertureDansLesLogs = true;
        }
        else{
            journaliseNonOuverture(uidPorte, lecteur.getUid(), lecteur.getBadgeUid());
            ErreurOuvertureDansLesLogs = true;
        }
    }
    
    private void journaliseNonOuverture(String uidPorte, String lecteur, String badgeUid) {
        LocalDateTime heure = LocalDateTime.now();
        String message = String.format("Non - Ouverture : Heure: %s, Porte: %s, Lecteur: %s, Badge lu: %s", heure, uidPorte, lecteur, badgeUid);
        logs.add(message);
    }

    public void journaliserOuverture(String porte, String lecteur, String badge) {
        LocalDateTime heure = LocalDateTime.now();
        String message = String.format("Ouverture : Heure: %s, Porte: %s, Lecteur: %s, Badge lu: %s", heure, porte, lecteur, badge);
        logs.add(message);
    }
    public String getLog(String UIDPorte) {
        for (String log : logs) {
            if (log.contains(UIDPorte)) {
                return log;
            }
        }
        return null;
    }
}
