package fr.epsi.controleacces.utilities;

import fr.epsi.controleacces.models.Badge;
import fr.epsi.controleacces.models.ILecteur;
import fr.epsi.controleacces.models.IPorte;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor
public class LecteurDummy implements ILecteur {
    private String uid;
    private String badgeUid;
    private List<IPorte> portes = new ArrayList<>();
    private boolean badgeDetecteAuProchainCycle = false;


    public LecteurDummy(String uid, List<IPorte> portes) {
        this.uid = uid;
        this.portes = portes;
    }
    @Override
    public boolean isBadgeDetecte() {
        throw new RuntimeException();
    }

    @Override
    public List<IPorte> getPortes() {
        return portes;
    }

    @Override
    public void simulerDetectionBadge(){
        badgeDetecteAuProchainCycle = false;
    }

    @Override
    public void simulerDetectionBadge(Badge badge){
        if(badge.isBloque()){
            badgeDetecteAuProchainCycle = true;
        }else{
            badgeDetecteAuProchainCycle = false;
        }
    }

    @Override
    public IPorte getPorte(String porteValide) {
        for(IPorte porte : portes){
            if(porte.getUid().equals(porteValide)){
                return porte;
            }
        }
        return null;
    }

    @Override
    public String getBadgeUid() {
        return badgeUid;
    }
}
