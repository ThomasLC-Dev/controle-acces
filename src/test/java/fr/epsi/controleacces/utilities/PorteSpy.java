package fr.epsi.controleacces.utilities;

import fr.epsi.controleacces.models.IPorte;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PorteSpy implements IPorte {
    private String uid;

    private boolean ARecuUnSignalDOuverture = false;

    public PorteSpy(String uid) {
        this.uid = uid;
    }
    public void ouvrir() {
        ARecuUnSignalDOuverture = true;
    }

    @Override
    public boolean getARecuUnSignalDOuverture() {
        return ARecuUnSignalDOuverture;
    }
}
