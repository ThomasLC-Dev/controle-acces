package fr.epsi.controleacces.utilities;

import fr.epsi.controleacces.models.Badge;
import fr.epsi.controleacces.models.ILecteur;
import fr.epsi.controleacces.models.IPorte;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class LecteurFake implements ILecteur {
    private String uid;
    private String badgeUid;
    private List<IPorte> portes = new ArrayList<>();
    private boolean badgeDetecteAuProchainCycle = false;
    public LecteurFake(String uid, List<IPorte> portes) {
        this.uid = uid;
        this.portes = portes;
    }

    public LecteurFake(String uid, IPorte porte) {
        this.uid = uid;
        this.portes.add(porte);
    }

    public void simulerDetectionBadge(){
        badgeDetecteAuProchainCycle = true;
    }

    public void simulerDetectionBadge(Badge badge){
        badgeUid = badge.getUid();
        if(badge.isBloque()){
            badgeDetecteAuProchainCycle = false;
        }else{
            badgeDetecteAuProchainCycle = true;
        }
    }

    @Override
    public IPorte getPorte(String porteValide) {
        for(IPorte porte : portes){
            if(porte.getUid().equals(porteValide)){
                return porte;
            }
        }
        return null;
    }

    public boolean isBadgeDetecte() {
        if (badgeDetecteAuProchainCycle) {
            badgeDetecteAuProchainCycle = false;
            return true;
        }
        return false;
    }
}