package fr.epsi.controleacces.utilities;

import fr.epsi.controleacces.models.ILecteur;
import fr.epsi.controleacces.models.IMoteurOuvertureLogger;
import fr.epsi.controleacces.models.IPorte;
import fr.epsi.controleacces.models.MoteurOuverture;

import java.util.ArrayList;
import java.util.List;

public class MoteurOuvertureBuilder {
    private List<ILecteur> lecteurs = new ArrayList<>();
    private IMoteurOuvertureLogger logger;

    public MoteurOuverture build() {
        MoteurOuverture moteurOuverture = new MoteurOuverture(lecteurs);
        moteurOuverture.setLogger(logger);
        return moteurOuverture;
    }

    public MoteurOuvertureBuilder withLogger() {
        this.logger = new LoggerSpy();
        return this;
    }

    public MoteurOuvertureBuilder withLecteurFonctionnel() {
        this.lecteurs.add(new LecteurFake());
        return this;
    }

    public MoteurOuvertureBuilder withLecteurFonctionnel(String uid) {
        this.lecteurs.add(new LecteurFake(uid, new ArrayList<>()));
        return this;
    }

    public MoteurOuvertureBuilder withLecteurDefaillant() {
        this.lecteurs.add(new LecteurDummy());
        return this;
    }

    public MoteurOuvertureBuilder withLecteurFonctionnel(String uid, List<IPorte> portes) {
        this.lecteurs.add(new LecteurFake(uid, portes));
        return this;
    }

    public MoteurOuvertureBuilder withLecteurDefaillant(String uid, List<IPorte> portes) {
        this.lecteurs.add(new LecteurDummy(uid, portes));
        return this;
    }

    public MoteurOuvertureBuilder espionnantUnePorteFonctionnelle(String nom){
        IPorte porte = new PorteSpy(nom);
        for(ILecteur lecteur : this.lecteurs){
            lecteur.getPortes().add(porte);
        }
        return this;
    }

    public MoteurOuvertureBuilder espionnantUnePorteDefaillante(String nom){
        IPorte porte = new PorteDummy(nom);
        for(ILecteur lecteur : this.lecteurs){
            lecteur.getPortes().add(porte);
        }
        return this;
    }
}
