package fr.epsi.controleacces.utilities;

import fr.epsi.controleacces.models.IPorte;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PorteDummy implements IPorte {
    private String uid;
    private boolean ARecuUnSignalDOuverture = false;

    public PorteDummy(String uid) {
        this.uid = uid;
    }
    @Override
    public void ouvrir() {
        throw new RuntimeException();
    }
    @Override
    public boolean getARecuUnSignalDOuverture() {
        return ARecuUnSignalDOuverture;
    }
}
