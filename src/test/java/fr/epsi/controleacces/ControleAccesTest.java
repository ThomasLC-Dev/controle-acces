package fr.epsi.controleacces;

import fr.epsi.controleacces.models.Badge;
import fr.epsi.controleacces.models.IPorte;
import fr.epsi.controleacces.models.MoteurOuverture;
import fr.epsi.controleacces.utilities.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ControleAccesTest {

    public static List<List<IPorte>> donneesTests (){
        List<List<IPorte>> donneesTests = new ArrayList<>();
        donneesTests.add(Arrays.asList(new PorteSpy("porte1")));
        donneesTests.add(Arrays.asList(new PorteSpy("porte1"), new PorteSpy("porte2")));
        return donneesTests;
    }

    /**
     * Cas Nominal
     */
    @Test
    void testCasNominal() {
        // ETANT DONNE un Lecteur de Badge relié à une Porte
        // ET un Moteur d'Ouverture interrogeant ce Lecteur
        MoteurOuverture moteur = new MoteurOuvertureBuilder()
                .withLecteurFonctionnel()
                .espionnantUnePorteFonctionnelle("porte1")
                .withLogger()
                .build();

        // QUAND un Badge valide est passé devant le Lecteur
        moteur.getLecteurs().get(0).simulerDetectionBadge();
        moteur.interrogerLecteurs();

        // ALORS un signal d'ouverture est envoyé à la Porte
        assertTrue(moteur.getLecteurs().get(0).getPorte("porte1").getARecuUnSignalDOuverture());

    }

    /**
     * Cas Aucun badge
     */
    @ParameterizedTest
    @MethodSource("donneesTests")
    void testCasAucunBadge(List<IPorte> portes) {
        // ETANT DONNE plusieurs Porte reliées à un Lecteur
        // ET un Moteur d'Ouverture interrogeant ce Lecteur
        MoteurOuverture moteur = new MoteurOuvertureBuilder()
                .withLecteurFonctionnel("lecteur1", portes)
                .withLogger()
                .build();

        // QUAND aucun Badge n'est passé devant le Lecteur
        moteur.interrogerLecteurs();

        // ALORS aucun signal d'ouverture n'est envoyé aux Portes
        for(IPorte porte : portes){
            assertFalse(porte.getARecuUnSignalDOuverture());
        }
    }

    @Test
    void testCasDeuxLecteur() {
        // ETANT DONNE deux Lecteurs de Badge reliés à une Porte
        // ET un Moteur d'Ouverture interrogeant ces Lecteurs
        PorteSpy porte = new PorteSpy("porte");
        MoteurOuverture moteur = new MoteurOuvertureBuilder()
                .withLecteurFonctionnel("lecteur1", List.of(porte))
                .withLecteurFonctionnel("lecteur2", List.of(porte))
                .withLogger()
                .build();

        // QUAND un Badge valide est passé devant l'un des deux Lecteurs
        moteur.getLecteurs().get(0).simulerDetectionBadge();
        moteur.interrogerLecteurs();

        // ALORS un signal d'ouverture est envoyé aux deux Portes
        assertTrue(porte.getARecuUnSignalDOuverture());
    }

    @Test
    void testCasDeuxLecteurAvecUnDefaillant(){
        // ETANT DONNE un Lecteur de Badge fonctionnel et un Lecteur de Badge défaillant reliés à une Porte
        // ET un Moteur d'Ouverture interrogeant ces Lecteurs
        PorteSpy porte = new PorteSpy("porte");
        MoteurOuverture moteur = new MoteurOuvertureBuilder()
                .withLecteurFonctionnel("lecteurFonctionnelle", List.of(porte))
                .withLecteurDefaillant("lecteurDefaillant", List.of(porte))
                .withLogger()
                .build();

        // QUAND un Badge valide est passé devant le Lecteur fonctionnelle
        moteur.getLecteur("lecteurFonctionnelle").simulerDetectionBadge();
        moteur.interrogerLecteurs();

        // ALORS un signal d'ouverture est envoyé à la Porte
        assertTrue(porte.getARecuUnSignalDOuverture());
    }

    @Test
    void testCasDeuxLecteurAvecUnDefaillantEtPassageDuBadgeSurCeLecteur(){
        // ETANT DONNE un Lecteur de Badge fonctionnel et un Lecteur de Badge défaillant reliés à une Porte
        // ET un Moteur d'Ouverture interrogeant ces Lecteurs
        PorteSpy porte = new PorteSpy("porte");
        MoteurOuverture moteur = new MoteurOuvertureBuilder()
                .withLecteurFonctionnel("lecteurFonctionnelle", List.of(porte))
                .withLecteurDefaillant("lecteurDefaillant", List.of(porte))
                .withLogger()
                .build();

        // QUAND un Badge valide est passé devant le Lecteur fonctionnelle
        moteur.getLecteur("lecteurDefaillant").simulerDetectionBadge();
        moteur.interrogerLecteurs();

        // ALORS un signal d'ouverture est envoyé à la Porte
        assertFalse(porte.getARecuUnSignalDOuverture());
    }

    /**
     * Cas Lecteur défaillant
     */
    @Test
    void testCasLecteurDefaillant() {
        // ETANT DONNE un Lecteur défaillant relié à une Porte
        MoteurOuverture moteur = new MoteurOuvertureBuilder()
                .withLecteurDefaillant()
                .espionnantUnePorteFonctionnelle("porte")
                .withLogger()
                .build();

        // QUAND on interroge ce Lecteur
        moteur.getLecteurs().get(0).simulerDetectionBadge();
        moteur.interrogerLecteurs();

        // ALORS aucun signal d'ouverture n'est envoyé à la Porte
        assertFalse(moteur.getLecteurs().get(0).getPorte("porte").getARecuUnSignalDOuverture());
        //  ET l'erreur est loguée
        assertTrue(moteur.getLogger().isExceptionDansLesLogs());
    }

    /**
     * Cas Badge bloqué
     */
    @ParameterizedTest
    @MethodSource("donneesTests")
    void testCasBadgeBloque(List<IPorte> portes) {
        // ETANT DONNE plusieurs Portes reliée à un Lecteur
        // ET un Badge ayant été bloqué
        MoteurOuverture moteur = new MoteurOuvertureBuilder()
                .withLecteurFonctionnel("lecteur1", portes)
                .withLogger()
                .build();
        Badge badgeBloque = new Badge();
        badgeBloque.bloquer();

        // QUAND on présente ce Badge à ce Lecteur
        moteur.getLecteurs().get(0).simulerDetectionBadge(badgeBloque);
        moteur.interrogerLecteurs();

        // ALORS les Portes ne reçoivent pas de signal d'ouverture
        for(IPorte porte : portes){
            assertFalse(porte.getARecuUnSignalDOuverture());
        }
    }

    /**
     * Cas Badge bloqué puis débloqué
    */
    @ParameterizedTest
    @MethodSource("donneesTests")
    void testCasBadgeBloquePuisDebloque( List<IPorte> portes) {
        // ETANT DONNE plusieurs Portes reliée à un Lecteur
        // ET un Badge ayant été bloqué puis débloqué
        MoteurOuverture moteur = new MoteurOuvertureBuilder()
                .withLecteurFonctionnel("lecteur1", portes)
                .withLogger()
                .build();
        Badge badge = new Badge();
        badge.bloquer();
        badge.debloquer();

        // QUAND on présente ce Badge à ce Lecteur
        moteur.getLecteurs().get(0).simulerDetectionBadge(badge);
        moteur.interrogerLecteurs();

        // ALORS les Portes reçoivent un signal d'ouverture
        for(IPorte porte : portes){
            assertTrue(porte.getARecuUnSignalDOuverture());
        }
    }

    /**
     * Cas une Porte fonctionnelle et une Porte défaillante
     */
    @Test
    void testCasUnePorteValideUnePorteDefaillante() {
        // ETANT DONNE une Porte défaillante et une Porte fonctionnelle reliées à un Lecteur
        MoteurOuverture moteur = new MoteurOuvertureBuilder()
                .withLecteurFonctionnel()
                .espionnantUnePorteFonctionnelle("porteValide")
                .espionnantUnePorteDefaillante("porteDefaillante")
                .withLogger()
                .build();


        // QUAND on interroge ce Lecteur
        moteur.getLecteurs().get(0).simulerDetectionBadge();
        moteur.interrogerLecteurs();

        // ALORS la Porte fonctionnelle reçoit un signal d'ouverture et pas la porte défaillante.
        assertTrue(moteur.getLecteurs().get(0).getPorte("porteValide").getARecuUnSignalDOuverture());
        assertFalse(moteur.getLecteurs().get(0).getPorte("porteDefaillante").getARecuUnSignalDOuverture());
    }

    @Test
    void testCasUnePorteValideUnePorteDefaillanteAvecBadgeBloque() {
        // ETANT DONNE une Porte défaillante et une Porte fonctionnelle reliées à un Lecteur
        MoteurOuverture moteur = new MoteurOuvertureBuilder()
                .withLecteurFonctionnel("lecteur")
                .espionnantUnePorteFonctionnelle("porteValide")
                .espionnantUnePorteDefaillante("porteDefaillante")
                .withLogger()
                .build();
        Badge badge = new Badge();
        badge.bloquer();

        // QUAND on interroge ce Lecteur avec un Badge bloqué
        moteur.getLecteur("lecteur").simulerDetectionBadge(badge);
        moteur.interrogerLecteurs();

        // ALORS aucune Porte ne reçoit de signal d'ouverture
        assertFalse(moteur.getLecteurs().get(0).getPorte("porteValide").getARecuUnSignalDOuverture());
        assertFalse(moteur.getLecteurs().get(0).getPorte("porteDefaillante").getARecuUnSignalDOuverture());
    }

    /**
     * Cas où aucun lecteur n'est connecté :
     */
    @Test
    void testCasAucunLecteurConnecte() {
        // ETANT DONNE aucun lecteur connecté au moteur d'ouverture
        MoteurOuverture moteur = new MoteurOuvertureBuilder()
                .withLogger()
                .build();
        LecteurFake lecteur = new LecteurFake("lecteur", List.of(new PorteSpy("porte") ));

        // QUAND un badge valide est présenté à un lecteur
        lecteur.simulerDetectionBadge();
        moteur.interrogerLecteurs();

        // ALORS aucun signal d'ouverture n'est envoyé aux portes
        assertFalse(lecteur.getPorte("porte").getARecuUnSignalDOuverture());
    }

    @Test
    void testCasAucunLecteurConnecteAvecBadgeValide() {
        // ETANT DONNE aucun lecteur connecté au moteur d'ouverture
        MoteurOuverture moteur = new MoteurOuvertureBuilder()
                .withLogger()
                .build();
        LecteurFake lecteur = new LecteurFake("lecteur", List.of(new PorteSpy("porte") ));

        // QUAND un badge valide est présenté à un lecteur
        lecteur.simulerDetectionBadge();
        moteur.interrogerLecteurs();

        // ALORS aucun signal d'ouverture n'est envoyé aux portes
        assertFalse(lecteur.getPorte("porte").getARecuUnSignalDOuverture());
    }

    @Test
    void testCasAucunLecteurConnecteAvecBadgeBloque() {
        // ETANT DONNE aucun lecteur connecté au moteur d'ouverture
        MoteurOuverture moteur = new MoteurOuvertureBuilder()
                .withLogger()
                .build();
        LecteurFake lecteur = new LecteurFake("lecteur", List.of(new PorteSpy("porte") ));
        Badge badge = new Badge();
        badge.bloquer();

        // QUAND un badge bloqué est présenté à un lecteur
        lecteur.simulerDetectionBadge(badge);
        moteur.interrogerLecteurs();

        // ALORS aucun signal d'ouverture n'est envoyé aux portes
        assertFalse(lecteur.getPorte("porte").getARecuUnSignalDOuverture());
    }

    @Test
    void testPlusieursBadgesValidesEnMemeTemps() {
        // ETANT DONNE un Lecteur de Badge relié à une Porte
        // ET un Moteur d'Ouverture interrogeant ce Lecteur
        MoteurOuverture moteur = new MoteurOuvertureBuilder()
                .withLecteurFonctionnel()
                .espionnantUnePorteFonctionnelle("porte1")
                .withLogger()
                .build();

        // QUAND plusieurs Badges valides sont passés devant le Lecteur
        moteur.getLecteurs().get(0).simulerDetectionBadge();
        moteur.getLecteurs().get(0).simulerDetectionBadge();
        moteur.interrogerLecteurs();

        // ALORS un signal d'ouverture est envoyé à la Porte
        assertTrue(moteur.getLecteurs().get(0).getPorte("porte1").getARecuUnSignalDOuverture());
    }

    @Test
    void testPlusieursBadgesValidesEnMemeTempsAvecDeuxPortes() {
        // ETANT DONNE un Lecteur de Badge relié à deux Portes
        // ET un Moteur d'Ouverture interrogeant ce Lecteur
        MoteurOuverture moteur = new MoteurOuvertureBuilder()
                .withLecteurFonctionnel()
                .espionnantUnePorteFonctionnelle("porte1")
                .espionnantUnePorteFonctionnelle("porte2")
                .withLogger()
                .build();

        // QUAND plusieurs Badges valides sont passés devant le Lecteur
        moteur.getLecteurs().get(0).simulerDetectionBadge();
        moteur.getLecteurs().get(0).simulerDetectionBadge();
        moteur.interrogerLecteurs();

        // ALORS un signal d'ouverture est envoyé aux deux Portes
        assertTrue(moteur.getLecteurs().get(0).getPorte("porte1").getARecuUnSignalDOuverture());
        assertTrue(moteur.getLecteurs().get(0).getPorte("porte2").getARecuUnSignalDOuverture());
    }

    @Test
    void testPlusieursBadgesValidesEnMemeTempsAvecDeuxPortesEtUnDefaillant() {
        // ETANT DONNE un Lecteur de Badge relié à deux Portes
        // ET un Moteur d'Ouverture interrogeant ce Lecteur
        MoteurOuverture moteur = new MoteurOuvertureBuilder()
                .withLecteurFonctionnel()
                .espionnantUnePorteFonctionnelle("porte1")
                .espionnantUnePorteFonctionnelle("porte2")
                .withLecteurDefaillant()
                .espionnantUnePorteDefaillante("porteDefaillante")
                .withLogger()
                .build();

        // QUAND plusieurs Badges valides sont passés devant le Lecteur
        moteur.getLecteurs().get(0).simulerDetectionBadge();
        moteur.getLecteurs().get(0).simulerDetectionBadge();
        moteur.interrogerLecteurs();

        // ALORS un signal d'ouverture est envoyé aux deux Portes
        assertTrue(moteur.getLecteurs().get(0).getPorte("porte1").getARecuUnSignalDOuverture());
        assertTrue(moteur.getLecteurs().get(0).getPorte("porte2").getARecuUnSignalDOuverture());
        assertFalse(moteur.getLecteurs().get(0).getPorte("porteDefaillante").getARecuUnSignalDOuverture());
    }

    @Test
    void testPlusieursBadgesValidesEnMemeTempsAvecDeuxPortesEtUnDefaillantEtUnBloque() {
        // ETANT DONNE un Lecteur de Badge relié à deux Portes
        // ET un Moteur d'Ouverture interrogeant ce Lecteur
        MoteurOuverture moteur = new MoteurOuvertureBuilder()
                .withLecteurFonctionnel()
                .espionnantUnePorteFonctionnelle("porte1")
                .espionnantUnePorteFonctionnelle("porte2")
                .withLecteurDefaillant()
                .espionnantUnePorteDefaillante("porteDefaillante")
                .withLogger()
                .build();

        // QUAND plusieurs Badges valides sont passés devant le Lecteur
        moteur.getLecteurs().get(0).simulerDetectionBadge();
        moteur.getLecteurs().get(0).simulerDetectionBadge(new Badge());
        moteur.interrogerLecteurs();

        // ALORS un signal d'ouverture est envoyé aux deux Portes
        assertTrue(moteur.getLecteurs().get(0).getPorte("porte1").getARecuUnSignalDOuverture());
        assertTrue(moteur.getLecteurs().get(0).getPorte("porte2").getARecuUnSignalDOuverture());
        assertFalse(moteur.getLecteurs().get(0).getPorte("porteDefaillante").getARecuUnSignalDOuverture());
    }

    @Test
    void testCasNominalAvecLogger() {
        // ETANT DONNEE une Porte fonctionnelle reliée à un Lecteur
        // ET un Moteur d'Ouverture
        MoteurOuverture moteur = new MoteurOuvertureBuilder()
                .withLecteurFonctionnel()
                .espionnantUnePorteFonctionnelle("porteFonctionnelle")
                .withLogger()
                .build();
        Badge badge = new Badge();

        // QUAND on interroge ce Lecteur
        moteur.getLecteurs().get(0).simulerDetectionBadge(badge);
        moteur.interrogerLecteurs();

        // ALORS un signal d'ouverture est envoyé à la Porte
        assertTrue(moteur.getLecteurs().get(0).getPorte("porteFonctionnelle").getARecuUnSignalDOuverture());

        // // ET on journalise l'ouverture de la porte avec les informations requises
        String log = moteur.getLogger().getLog("porteFonctionnelle");
        assertTrue(log.contains("Ouverture")); // Vérifie la présence du message d'ouverture dans le log
        assertTrue(log.contains("Heure:")); // Vérifie la présence de l'heure dans le log
        assertTrue(log.contains("Porte: porteFonctionnelle")); // Vérifie la présence de la porte concernée dans le log
        assertTrue(log.contains("Lecteur:")); // Vérifie la présence du lecteur dans le log
        assertTrue(log.contains("Badge lu:")); // Vérifie la présence du badge lu dans le log
    }

    @Test
    void testPorteFonctionnelleEtPorteDefaillanteAvecLogger() {
        // ETANT DONNEE une Porte fonctionnelle et une porte défaillante reliées à un Lecteur
        MoteurOuverture moteur = new MoteurOuvertureBuilder()
                .withLecteurFonctionnel()
                .espionnantUnePorteFonctionnelle("porteFonctionnelle")
                .espionnantUnePorteDefaillante("porteDefaillante")
                .withLogger()
                .build();

        // QUAND on interroge ce Lecteur
        moteur.getLecteurs().get(0).simulerDetectionBadge();
        moteur.interrogerLecteurs();

        // ALORS un signal d'ouverture est envoyé aux portes
        assertTrue(moteur.getLecteurs().get(0).getPorte("porteFonctionnelle").getARecuUnSignalDOuverture());
        assertFalse(moteur.getLecteurs().get(0).getPorte("porteDefaillante").getARecuUnSignalDOuverture());

        // ET on journalise l'ouverture pour la porte fonctionnelle
        String log = moteur.getLogger().getLog("porteFonctionnelle");
        assertTrue(log.contains("Ouverture"));
        assertTrue(log.contains("Heure:"));
        assertTrue(log.contains("Porte: porteFonctionnelle"));
        assertTrue(log.contains("Lecteur:"));
        assertTrue(log.contains("Badge lu:"));

        //ET on journalise l'erreur pour la porte défaillante
        log = moteur.getLogger().getLog("porteDefaillante");
        assertTrue(log.contains("Non - Ouverture"));
        assertTrue(log.contains("Heure:"));
        assertTrue(log.contains("Porte: porteDefaillante"));
        assertTrue(log.contains("Lecteur:"));
        assertTrue(log.contains("Badge lu:"));
    }

    @Test
    void testCasNominalAvecLoggerEtBadgeBloque() {
        // ETANT DONNEE une Porte fonctionnelle reliée à un Lecteur
        // ET un Moteur d'Ouverture
        MoteurOuverture moteur = new MoteurOuvertureBuilder()
                .withLecteurFonctionnel()
                .espionnantUnePorteFonctionnelle("porteFonctionnelle")
                .withLogger()
                .build();
        Badge badge = new Badge();
        badge.bloquer();

        // QUAND on interroge ce Lecteur
        moteur.getLecteurs().get(0).simulerDetectionBadge(badge);
        moteur.interrogerLecteurs();

        // ALORS aucun signal d'ouverture n'est envoyé à la Porte
        assertFalse(moteur.getLecteurs().get(0).getPorte("porteFonctionnelle").getARecuUnSignalDOuverture());

        // ET on journalise l'erreur
        String log = moteur.getLogger().getLog("Aucune porte");
        assertTrue(log.contains("Non - Ouverture"));
        assertTrue(log.contains("Heure:"));
        assertTrue(log.contains("Porte: Aucune porte"));
        assertTrue(log.contains("Lecteur:"));
        assertTrue(log.contains("Badge lu:"));
    }

    @Test
    void testCasNominalAvecLoggerEtBadgeBloquePuisDebloque() {
        // ETANT DONNEE une Porte fonctionnelle reliée à un Lecteur
        // ET un Moteur d'Ouverture
        MoteurOuverture moteur = new MoteurOuvertureBuilder()
                .withLecteurFonctionnel()
                .espionnantUnePorteFonctionnelle("porteFonctionnelle")
                .withLogger()
                .build();
        Badge badge = new Badge();
        badge.bloquer();
        badge.debloquer();

        // QUAND on interroge ce Lecteur
        moteur.getLecteurs().get(0).simulerDetectionBadge(badge);
        moteur.interrogerLecteurs();

        // ALORS un signal d'ouverture est envoyé à la Porte
        assertTrue(moteur.getLecteurs().get(0).getPorte("porteFonctionnelle").getARecuUnSignalDOuverture());

        // ET on journalise l'ouverture
        String log = moteur.getLogger().getLog("porteFonctionnelle");
        assertTrue(log.contains("Ouverture"));
        assertTrue(log.contains("Heure:"));
        assertTrue(log.contains("Porte: porteFonctionnelle"));
        assertTrue(log.contains("Lecteur:"));
        assertTrue(log.contains("Badge lu:"));
    }

    @Test
    void testCasNominalAvecLoggerEtBadgeBloquePuisDebloqueAvecDeuxPortes() {
        // ETANT DONNEE deux Portes fonctionnelles reliées à un Lecteur
        // ET un Moteur d'Ouverture
        MoteurOuverture moteur = new MoteurOuvertureBuilder()
                .withLecteurFonctionnel()
                .espionnantUnePorteFonctionnelle("porteFonctionnelle")
                .espionnantUnePorteFonctionnelle("porteFonctionnelle2")
                .withLogger()
                .build();
        Badge badge = new Badge();
        badge.bloquer();
        badge.debloquer();

        // QUAND on interroge ce Lecteur
        moteur.getLecteurs().get(0).simulerDetectionBadge(badge);
        moteur.interrogerLecteurs();

        // ALORS un signal d'ouverture est envoyé aux Portes
        assertTrue(moteur.getLecteurs().get(0).getPorte("porteFonctionnelle").getARecuUnSignalDOuverture());
        assertTrue(moteur.getLecteurs().get(0).getPorte("porteFonctionnelle2").getARecuUnSignalDOuverture());

        // ET on journalise l'ouverture
        String log = moteur.getLogger().getLog("porteFonctionnelle");
        assertTrue(log.contains("Ouverture"));
        assertTrue(log.contains("Heure:"));
        assertTrue(log.contains("Porte: porteFonctionnelle"));
        assertTrue(log.contains("Lecteur:"));
        assertTrue(log.contains("Badge lu:"));

        log = moteur.getLogger().getLog("porteFonctionnelle2");
        assertTrue(log.contains("Ouverture"));
        assertTrue(log.contains("Heure:"));
        assertTrue(log.contains("Porte: porteFonctionnelle2"));
        assertTrue(log.contains("Lecteur:"));
        assertTrue(log.contains("Badge lu:"));
    }

    @Test
    void testAvecLoggerEtBadgeBloquePuisDebloqueAvecDeuxPortesEtUnDefaillant() {
        // ETANT DONNEE deux Porte fonctionnelle et une porte défaillante reliée à un Lecteur
        // ET un Moteur d'Ouverture
        MoteurOuverture moteur = new MoteurOuvertureBuilder()
                .withLecteurFonctionnel()
                .espionnantUnePorteFonctionnelle("porteFonctionnelle")
                .espionnantUnePorteFonctionnelle("porteFonctionnelle2")
                .withLecteurDefaillant()
                .espionnantUnePorteDefaillante("porteDefaillante")
                .withLogger()
                .build();
        Badge badge = new Badge();
        badge.bloquer();
        badge.debloquer();

        // QUAND on interroge ce Lecteur
        moteur.getLecteurs().get(0).simulerDetectionBadge(badge);
        moteur.interrogerLecteurs();

        // ALORS un signal d'ouverture est envoyé aux Portes fonctionnelles
        assertTrue(moteur.getLecteurs().get(0).getPorte("porteFonctionnelle").getARecuUnSignalDOuverture());
        assertTrue(moteur.getLecteurs().get(0).getPorte("porteFonctionnelle2").getARecuUnSignalDOuverture());
        assertFalse(moteur.getLecteurs().get(0).getPorte("porteDefaillante").getARecuUnSignalDOuverture());

        // ET on journalise l'ouverture
        String log = moteur.getLogger().getLog("porteFonctionnelle");
        assertTrue(log.contains("Ouverture"));
        assertTrue(log.contains("Heure:"));
        assertTrue(log.contains("Porte: porteFonctionnelle"));
        assertTrue(log.contains("Lecteur:"));
        assertTrue(log.contains("Badge lu:"));

        log = moteur.getLogger().getLog("porteFonctionnelle2");
        assertTrue(log.contains("Ouverture"));
        assertTrue(log.contains("Heure:"));
        assertTrue(log.contains("Porte: porteFonctionnelle2"));
        assertTrue(log.contains("Lecteur:"));
        assertTrue(log.contains("Badge lu:"));
    }

}
